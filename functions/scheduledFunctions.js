const functions = require('firebase-functions')
const { log, error } = require('firebase-functions/lib/logger')

const admin = require('./admin')
const { calculateMinutesDiff, convertExpiresInMillisToDate } = require('./utils')
const { pubSubClient } = require('./pubSub')
const {
  EIGHT_DAYS_MILLIS,
  INSTAGRAM_REFRESH_PUBSUB_TOPIC,
  TIME_CHECK_INFOS_COLLECTION,
  USER_CHECK_PUBSUB_TOPIC,
  USER_PROFILES_COLLECTION
} = require('./constants')

exports.gatherUsersToCheck = functions.runWith({ timeoutSeconds: 360, memory: '1GB' })
  .pubsub.schedule('every 6 minutes').onRun(async _ => {
    log('Starting gathering users to check!')
    const prevDate = new Date()
    prevDate.setUTCHours(0, 0, 0, 0)
    const snapshot = await admin.firestore()
      .collection(TIME_CHECK_INFOS_COLLECTION)
      .where('active', '==', true)
      .where('lastSuccessfulCheck', '<', prevDate)
      .get()

    if (snapshot.empty) {
      log('No users to check!')
      return
    }

    const nowDate = new Date()
    snapshot.forEach(async doc => {
      try {
        log('Got user to try!', { extra: { userId: doc.id } })
        const docData = doc.data()
        const minutesDiff = calculateMinutesDiff(nowDate, docData.time.toDate())
        if (minutesDiff >= 0 && minutesDiff <= 31) {
          log('User should be checked!', { extra: { userId: doc.id } })
          const dataBuffer = Buffer.from(JSON.stringify({ userId: doc.id }))
          const messageId = await pubSubClient.topic(USER_CHECK_PUBSUB_TOPIC).publish(dataBuffer)
          log('Message published!', { extra: { messageId } })
        } else {
          log('User should not be checked!', { extra: { userId: doc.id } })
        }
      } catch (err) {
        error('Received error while publishing message!', { extra: { error: err } })
      }
    })
  })

// given maximum timeout and memory limits
exports.gatherTokensToRefresh = functions.runWith({ timeoutSeconds: 540, memory: '1GB' })
  .pubsub.schedule('every monday 15:00').onRun(async _ => {
    const snapshot = await admin.firestore()
      .collection(USER_PROFILES_COLLECTION)
      .where(
        'instagramData.expDate',
        '<=',
        admin.firestore.Timestamp.fromDate(convertExpiresInMillisToDate(EIGHT_DAYS_MILLIS))
      )
      .get()

    if (snapshot.empty) {
      log('No tokens to refresh.')
      return
    }

    snapshot.forEach(async doc => {
      try {
        log('Got token to refresh', { extra: { userId: doc.id } })
        const dataBuffer = Buffer.from(JSON.stringify({ userId: doc.id }))
        const messageId = await pubSubClient.topic(INSTAGRAM_REFRESH_PUBSUB_TOPIC).publish(dataBuffer)
        log(`Message ${messageId} published.`)
      } catch (err) {
        error('Received error while publishing message!', { extra: { error: err } })
      }
    })
  })
