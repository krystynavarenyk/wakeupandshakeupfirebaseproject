function calculateMinutesDiff (date, otherDate) {
  return (date.getUTCHours() * 60 + date.getUTCMinutes()) -
        (otherDate.getUTCHours() * 60 + otherDate.getUTCMinutes())
}

function convertExpiresInMillisToDate (expiresIn) {
  const expirationDate = new Date((new Date()).getTime() + expiresIn)
  return expirationDate
}

function defineConstProperty (obj, name, value) {
  Object.defineProperty(obj, name, {
    value: value,
    writable: false,
    enumerable: true,
    configurable: true
  })
}

function createMessage (mode) {
  let text = ''
  if (mode === 'polite') {
    text = 'Yeah! It\'s time to work out! Make your day better!'
  } else if (mode === 'rude') {
    text = 'Get up your lazy ass. You have to exercise more and eat less!'
  } else {
    throw new Error(`Unrecognised mode: ${mode}`)
  }
  return {
    subject: 'WakeUpAndShakeUp Reminder',
    text: text
  }
}

function isSameDate (date, otherDate) {
  return date.getDate() === otherDate.getDate() &&
    date.getMonth() === otherDate.getMonth() &&
    date.getFullYear() === otherDate.getFullYear()
}

exports.calculateMinutesDiff = calculateMinutesDiff
exports.createMessage = createMessage
exports.convertExpiresInMillisToDate = convertExpiresInMillisToDate
exports.defineConstProperty = defineConstProperty
exports.isSameDate = isSameDate
