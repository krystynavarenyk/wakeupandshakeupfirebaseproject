const { defineConstProperty } = require('./utils')

defineConstProperty(exports, 'CHECK_HASHTAG', '#WakeUpAndShakeUp')
defineConstProperty(exports, 'EIGHT_DAYS_MILLIS', 8 * 24 * 60 * 60 * 1000)
defineConstProperty(exports, 'INSTAGRAM_REFRESH_PUBSUB_TOPIC', 'instagramRefresh')
defineConstProperty(exports, 'MAILING_COLLECTION', 'mail')
defineConstProperty(exports, 'TIME_CHECK_INFOS_COLLECTION', 'timeCheckInfos')
defineConstProperty(exports, 'USER_CHECK_PUBSUB_TOPIC', 'userCheck')
defineConstProperty(exports, 'USER_PROFILES_COLLECTION', 'userProfiles')
