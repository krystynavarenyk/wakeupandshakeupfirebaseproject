const https = require('https')
const querystring = require('querystring')
const { log, error } = require('firebase-functions/lib/logger')

function getInstagramLongLivingToken (clientSecret, accessToken) {
  log('Getting instagram token!')
  const pathParams = { client_secret: clientSecret, access_token: accessToken, grant_type: 'ig_exchange_token' }
  const options = {
    host: 'graph.instagram.com',
    method: 'GET',
    path: `/access_token?${querystring.stringify(pathParams)}`
  }
  return new Promise((resolve, reject) => {
    const instagramRequest = https.request(options, (res) => {
      log('Status code', { extra: { statusCode: res.statusCode } })
      let dataJson
      let data = ''
      res.on('data', (dataChunk) => {
        data += dataChunk
      })
      res.on('end', () => {
        log('Received data!', { extra: { data } })
        dataJson = JSON.parse(data)
        if (res.statusCode >= 200 && res.statusCode < 300) {
          try {
            const result = {
              accessToken: dataJson.access_token,
              expiresIn: dataJson.expires_in
            }
            resolve(result)
          } catch (err) {
            error('Error while parsing long living token response!', { extra: { error: err } })
            reject(err)
          }
        } else {
          reject(new Error(dataJson.error_message))
        }
      })
      res.on('error', (err) => {
        error('Error while getting long living token!', { extra: { error: err } })
        reject(err)
      })
    })
    instagramRequest.end()
  })
}

function getInstagramShortLivingToken (clientId, clientSecret, redirectUri, code) {
  const postData = querystring.stringify({
    client_id: clientId,
    client_secret: clientSecret,
    grant_type: 'authorization_code',
    redirect_uri: redirectUri,
    code: code
  })
  const options = {
    host: 'api.instagram.com',
    method: 'POST',
    path: '/oauth/access_token',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Content-Length': postData.length
    }
  }
  return new Promise((resolve, reject) => {
    const instagramRequest = https.request(options, (res) => {
      log('Status code', { extra: { statusCode: res.statusCode } })
      let dataJson
      let data = ''
      res.on('data', (dataChunk) => {
        data += dataChunk
      })
      res.on('end', () => {
        log('Received data!', { extra: { data } })
        dataJson = JSON.parse(data)
        if (res.statusCode >= 200 && res.statusCode < 300) {
          try {
            const result = {
              accessToken: dataJson.access_token
            }
            resolve(result)
          } catch (err) {
            error(`Error while parsing short living token response: ${err}`)
            reject(err)
          }
        } else {
          reject(new Error(dataJson.error.message))
        }
      })
      res.on('error', (err) => {
        error('Error while getting short living token!', { extra: { error: err } })
        reject(err)
      })
    })
    instagramRequest.end(postData)
  })
}

function getInstagramUserData (accessToken) {
  const pathParams = { access_token: accessToken, fields: 'id,username' }
  const options = {
    host: 'graph.instagram.com',
    method: 'GET',
    path: `/me?${querystring.stringify(pathParams)}`
  }
  return new Promise((resolve, reject) => {
    const instagramRequest = https.request(options, (res) => {
      log('Status code', { extra: { statusCode: res.statusCode } })
      let dataJson
      let data = ''
      res.on('data', (dataChunk) => {
        data += dataChunk
      })
      res.on('end', () => {
        log('Received data!', { extra: { data } })
        dataJson = JSON.parse(data)
        if (res.statusCode >= 200 && res.statusCode < 300) {
          try {
            const result = {
              id: dataJson.id,
              username: dataJson.username
            }
            resolve(result)
          } catch (err) {
            error('Error while parsing instagram user data response!', { extra: { error: err } })
            reject(err)
          }
        } else {
          reject(new Error(dataJson.error.message))
        }
      })
      res.on('error', (err) => {
        error('Error while getting instagram user data!', { extra: { error: err } })
        reject(err)
      })
    })
    instagramRequest.end()
  })
}

function getUserMedias (instagramUserId, accessToken) {
  const pathParams = { access_token: accessToken, fields: 'id,caption,media_type,media_url,username,timestamp' }
  const options = {
    host: 'graph.instagram.com',
    method: 'GET',
    path: `/${instagramUserId}/media?${querystring.stringify(pathParams)}`
  }
  return new Promise((resolve, reject) => {
    const instagramRequest = https.request(options, (res) => {
      log('Status code', { extra: { statusCode: res.statusCode } })
      let dataJson
      let data = ''
      res.on('data', (dataChunk) => {
        data += dataChunk
      })
      res.on('end', () => {
        log('Received data!', { extra: { data } })
        dataJson = JSON.parse(data)
        if (res.statusCode >= 200 && res.statusCode < 300) {
          try {
            resolve(dataJson.data)
          } catch (err) {
            error('Error while parsing instagram user media response!', { extra: { error: err } })
            reject(err)
          }
        } else {
          reject(new Error(dataJson.error.message))
        }
      })
      res.on('error', (err) => {
        error('Error while getting instagram user media!', { extra: { error: err } })
        reject(err)
      })
    })
    instagramRequest.end()
  })
}

function refreshInstagramLongLivingToken (accessToken) {
  const pathParams = { access_token: accessToken, grant_type: 'ig_refresh_token' }
  const options = {
    host: 'graph.instagram.com',
    method: 'GET',
    path: `/refresh_access_token?${querystring.stringify(pathParams)}`
  }
  return new Promise((resolve, reject) => {
    const instagramRequest = https.request(options, (res) => {
      log('Status code', { extra: { statusCode: res.statusCode } })
      let dataJson
      let data = ''
      res.on('data', (dataChunk) => {
        data += dataChunk
      })
      res.on('end', () => {
        log('Received data!', { extra: { data } })
        dataJson = JSON.parse(data)
        if (res.statusCode >= 200 && res.statusCode < 300) {
          try {
            const result = {
              accessToken: dataJson.access_token,
              expiresIn: dataJson.expires_in
            }
            resolve(result)
          } catch (err) {
            error('Error while parsing refresh token response!', { extra: { error: err } })
            reject(err)
          }
        } else {
          reject(new Error(dataJson.error.message))
        }
      })
      res.on('error', (err) => {
        error('Error while refreshing token!', { extra: { error: err } })
        reject(err)
      })
    })
    instagramRequest.end()
  })
}

exports.getInstagramLongLivingToken = getInstagramLongLivingToken
exports.getInstagramShortLivingToken = getInstagramShortLivingToken
exports.getInstagramUserData = getInstagramUserData
exports.getUserMedias = getUserMedias
exports.refreshInstagramLongLivingToken = refreshInstagramLongLivingToken
