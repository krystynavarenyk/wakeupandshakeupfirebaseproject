const ejs = require('ejs')
const functions = require('firebase-functions')
const { log, error } = require('firebase-functions/lib/logger')

const admin = require('./admin')
const { convertExpiresInMillisToDate } = require('./utils')
const { instagramConfig } = require('./configs')
const { USER_PROFILES_COLLECTION } = require('./constants')
const {
  getInstagramShortLivingToken,
  getInstagramLongLivingToken,
  getInstagramUserData
} = require('./instagramFunctions')

exports.authInstagramUser = functions.https.onRequest(async (req, res) => {
  log('Authenticate instagram user!')
  const code = req.query.code.split('#_')[0]
  const userId = req.query.state
  try {
    log('Get short living token!')
    const shortLivingTokenRes = await getInstagramShortLivingToken(
      instagramConfig.client_id,
      instagramConfig.client_secret,
      instagramConfig.redirect_uri,
      code
    )
    log('Get long living token!')
    const longLivingTokenRes = await getInstagramLongLivingToken(
      instagramConfig.client_secret,
      shortLivingTokenRes.accessToken
    )
    // `expiresIn` is in seconds, but function expects it to be in milliseconds
    const expirationDate = convertExpiresInMillisToDate(longLivingTokenRes.expiresIn * 1000)
    log('Get instagram user profile data!')
    const userData = await getInstagramUserData(longLivingTokenRes.accessToken)
    const instagramData = {
      token: longLivingTokenRes.accessToken,
      id: userData.id,
      username: userData.username,
      expDate: admin.firestore.Timestamp.fromDate(expirationDate)
    }
    log('Saving instagram data!')
    await admin
      .firestore()
      .collection(USER_PROFILES_COLLECTION)
      .doc(userId)
      .set({ instagramData }, { merge: true })
    log('Successfully saved auth token!')
    ejs.renderFile('./assets/authInstagramUserSuccess.ejs', { username: userData.username }, (err, str) => {
      if (err) {
        res.status(500).send({ error: err })
      } else {
        res.send(str)
      }
    })
  } catch (err) {
    error('Error occured while getting auth token!', { extra: { error: err } })
    res.status(500).send({ error: err })
  }
})
