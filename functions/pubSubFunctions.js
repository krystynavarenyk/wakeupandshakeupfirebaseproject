const functions = require('firebase-functions')
const { log, error, warn } = require('firebase-functions/lib/logger')

const admin = require('./admin')
const { convertExpiresInMillisToDate, createMessage, isSameDate } = require('./utils')
const { getUserMedias, refreshInstagramLongLivingToken } = require('./instagramFunctions')
const {
  CHECK_HASHTAG,
  INSTAGRAM_REFRESH_PUBSUB_TOPIC,
  MAILING_COLLECTION,
  TIME_CHECK_INFOS_COLLECTION,
  USER_CHECK_PUBSUB_TOPIC,
  USER_PROFILES_COLLECTION
} = require('./constants')

exports.checkUserMedia = functions.pubsub.topic(USER_CHECK_PUBSUB_TOPIC).onPublish(async message => {
  const userId = message.json.userId
  log('Got user to check!', { extra: { userId } })
  const db = admin.firestore()
  const now = new Date()
  // first check if user has not already passed the check today
  const userTimeCheckInfoRef = db.collection(TIME_CHECK_INFOS_COLLECTION).doc(userId)
  const userTimeCheckInfoDoc = await userTimeCheckInfoRef.get()
  if (!userTimeCheckInfoDoc.exists) {
    error('No time check info for user!', { extra: { userId: userId } })
    return
  }
  if (isSameDate(now, userTimeCheckInfoDoc.data().lastSuccessfulCheck.toDate())) {
    warn('User already passed check today!', { extra: { userId: userId } })
  }
  // check if profile exists and has access token
  const userProfileDocRef = db.collection(USER_PROFILES_COLLECTION).doc(userId)
  const userProfileDoc = await userProfileDocRef.get()
  if (!userProfileDoc.exists) {
    error('No profile for user!', { extra: { userId: userId } })
    return
  }
  const accessToken = userProfileDoc.data().instagramData.token
  if (!accessToken) {
    error('User has not access token!', { extra: { userId: userId } })
    return
  }
  // check media but only those, which were posted today
  const instagramId = userProfileDoc.data().instagramData.id
  const userMedias = await getUserMedias(instagramId, accessToken)
  const todayStart = new Date()
  todayStart.setUTCHours(0, 0, 0, 0)
  const relevantMedias = userMedias.filter(media => {
    const dataTimestamp = new Date(media.timestamp)
    return dataTimestamp >= todayStart
  })
  // if user posted new photo with needed hashtag it means that check is successful
  if (relevantMedias.some(media => media.caption.includes(CHECK_HASHTAG))) {
    log('User with did pass check!', { extra: { userId } })
    await db.collection(TIME_CHECK_INFOS_COLLECTION).doc(userId).set({
      lastSuccessfulCheck: now,
      successfulChecks: admin.firestore.FieldValue.arrayUnion(now)
    }, { merge: true })
    await userProfileDocRef.set({ status: 'active' }, { merge: true })
  } else {
    log('User with did not pass check!', { extra: { userId } })
    // 1 get user's email
    try {
      const userEmail = (await admin.auth().getUser(userId)).email
      // 2 create mail data with atributes
      const mailData = {
        message: createMessage(userProfileDoc.data().mode || 'polite'),
        to: [userEmail]
      }
      // 3 write in to needed collection
      const res = await db.collection(MAILING_COLLECTION).add(mailData)
      log('Added mail to be sent!', { extra: { mailId: res.id } })
    } catch (err) {
      error('Error fetching user data:', { extra: { err } })
    }
    await userProfileDocRef.set({ status: 'lazy' }, { merge: true })
  }
})

exports.refreshInstagramToken = functions.pubsub.topic(INSTAGRAM_REFRESH_PUBSUB_TOPIC).onPublish(async message => {
  const userId = message.json.userId
  log('Refreshing user instagram token', { extra: { userId } })
  const userProfileDocRef = admin.firestore().collection(USER_PROFILES_COLLECTION).doc(userId)
  const userProfileDoc = await userProfileDocRef.get()
  if (!userProfileDoc.exists) {
    throw new Error(`No document with id: ${userId}!`)
  } else {
    const { instagramData } = userProfileDoc.data()
    await userProfileDocRef.set({
      instagramData: {
        token: null,
        expDate: null
      }
    }, { merge: true })
    const refreshedLongLivingTokenRes = await refreshInstagramLongLivingToken(instagramData.token)
    // `expiresIn` is in seconds, but function expects it to be in milliseconds
    const expDate = convertExpiresInMillisToDate(refreshedLongLivingTokenRes.expiresIn * 1000)
    const refreshedInstagramData = {
      token: refreshedLongLivingTokenRes.accessToken,
      expDate: admin.firestore.Timestamp.fromDate(expDate)
    }
    await userProfileDocRef.set({ instagramData: refreshedInstagramData }, { merge: true })
    log('Successfully saved refreshed auth token', { extra: { userId } })
  }
})
