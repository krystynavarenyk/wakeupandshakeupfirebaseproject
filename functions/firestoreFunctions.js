const functions = require('firebase-functions')
const { log } = require('firebase-functions/lib/logger')

const admin = require('./admin')
const {
  TIME_CHECK_INFOS_COLLECTION,
  USER_PROFILES_COLLECTION
} = require('./constants')

// Add `lastSuccessfulCheck` field to new timeCheckInfo document
exports.onCreateTimeCheckInfoAddLastSuccessfulCheck = functions.firestore
  .document(`${TIME_CHECK_INFOS_COLLECTION}/{userId}`)
  .onCreate((snapshot, context) => {
    const { userId } = context.params
    log('Got new timeCheckInfo!', { extra: { userId } })
    return snapshot.ref.set(
      { lastSuccessfulCheck: new admin.firestore.Timestamp(0, 0) },
      { merge: true }
    )
  })

// Add `mode` and `status` fields to new userProfile document
exports.onCreateUserProfileAddModeAndStatus = functions.firestore
  .document(`${USER_PROFILES_COLLECTION}/{userId}`)
  .onCreate((snapshot, context) => {
    const { userId } = context.params
    log('Got new userProfile!', { extra: { userId } })
    return snapshot.ref.set(
      { mode: 'polite', status: 'active' },
      { merge: true }
    )
  })
