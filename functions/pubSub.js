const { PubSub } = require('@google-cloud/pubsub')
const { defineConstProperty } = require('./utils')

defineConstProperty(exports, 'pubSubClient', new PubSub())
